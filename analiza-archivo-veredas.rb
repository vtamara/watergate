#!/usr/bin/env ruby
# encoding: utf-8
#
# Dominio público de acuerdo a la legislazción colombiana.
# vtamara@pasosdeJesus.org 2019
#
# Analiza archivo con veredas respecto a un DIVIPOLA

require 'byebug'
require 'csv'

if ARGV.length != 2
  STDERR.puts "Primer argumento debe ser DIVIPOLA con campos departamento,municipio,centropoblado"
  STDERR.puts "Segundo debe ser archivo de veredas con campos departamento,municipio,vereda,cod_vereda"
  exit 1
end

def normaliza(texto)
  if texto.nil? || texto == '' || texto == 'null'
    return nil
  end
  r = texto.downcase.gsub('á', 'a')
  r.gsub!('Á', 'a')
  r.gsub!('é', 'e')
  r.gsub!('É', 'e')
  r.gsub!('í', 'i')
  r.gsub!('Í', 'i')
  r.gsub!('ó', 'o')
  r.gsub!('Ó', 'o')
  r.gsub!('ú', 'u')
  r.gsub!('Ú', 'u')
  r.gsub!('ü', 'u')
  r.gsub!('Ü', 'u')
  r
end

def a_mayusculas(texto)
  if texto.nil? || texto == '' || texto == 'null'
    return ''
  end
  r = texto.upcase
  r.gsub!('á', 'Á')
  r.gsub!('é', 'É')
  r.gsub!('í', 'Í')
  r.gsub!('ó', 'Ó')
  r.gsub!('ú', 'Ú')
  r.gsub!('ü', 'Ü')
  r
end



ndiv = ARGV[0]
STDERR.puts "Leyendo divipola #{ndiv}"
divcsv = CSV.read(ndiv, headers: true)
encdiv = divcsv[0].headers
STDERR.puts "Se leyeron #{divcsv.count} registros de #{ndiv}"
#Sin normalizar
#divcsv.each do |r|
#  r['departamento'] = normaliza(r['departamento'])
#  r['municipio'] = normaliza(r['municipio'])
#  r['centropoblado'] = normaliza(r['centropoblado'])
#end
#STDERR.puts "Divipola normalizado"

adiv = {} 
divcsv.each do |c|
  if !c['departamento'] || !c['municipio'] || !c['centropoblado']
    STDERR.puts "Registro sin departamento o municipio o centropoblado"
    exit 1
  end
  cdep = a_mayusculas(c['departamento'])
  if !adiv[cdep]
    adiv[cdep] = {}
    adiv[cdep][c['municipio']] = {}
  elsif !adiv[cdep][c['municipio']]
    adiv[cdep][c['municipio']] = {}
  end
  adiv[cdep][c['municipio']][c['centropoblado']] = c['cod_centropoblado'] 
end
STDERR.puts "Creado divipola estilo arbol"


nver = ARGV[1]
STDERR.puts "Leyendo archivo con veredas #{nver}"
vercsv = CSV.read(nver, headers: true)
encver = vercsv[0].headers
STDERR.puts "Se leyeron #{vercsv.count} registros de #{nver}"

maldep = 0
malmun = 0
vrep = 0
nver = 0
nreg = 0
vercsv.each do |r|
  nreg += 1
# Sin normalizar
# r['departamento'] = normaliza(r['departamento'])
#  r['municipio'] = normaliza(r['municipio'])
#  r['vereda'] = normaliza(r['vereda'])
  if !adiv[r['departamento']]
    puts "#{r['cod_municipio']},\"#{r['departamento'].upcase}\",\"#{r['municipio'].upcase}\",#{r['cod_vereda']},\"#{r['vereda'].upcase}\",No se encontró departamento"
    maldep += 1
  elsif !adiv[r['departamento']][r['municipio']]
    puts "#{r['cod_municipio']},\"#{r['departamento']}\",\"#{r['municipio']}\",#{r['cod_vereda']},\"#{r['vereda'].upcase}\",No se encontró municipio"
    malmun += 1
  elsif adiv[r['departamento']][r['municipio']][r['vereda']]
    puts "#{r['cod_municipio']},\"#{r['departamento'].upcase}\",\"#{r['municipio'].upcase}\",#{r['cod_vereda']},\"#{r['vereda'].upcase}\",Centro poblado #{adiv[r['departamento']][r['municipio']][r['vereda']]} en DIVIPOLA tiiene el mismo nombre de la vereda"
    vrep += 1
  end
end
STDERR.puts "Veredas normalizadas"  
STDERR.puts "Registros con departamento no encontrado en DIVIPOLA: #{maldep}"
STDERR.puts "Registros con municipio no encontrado en DIVIPOLA: #{malmun}"
STDERR.puts "Centros poblado con el mismo nombre de una vereda en el mismo municipio: #{vrep}"

aver = {} 
nreg = 0
vrep = 0
vercsv.each do |c|
  nreg += 1
  if !aver[c['departamento']]
    aver[c['departamento']] = {}
    aver[c['departamento']][c['municipio']] = {}
  elsif !aver[c['departamento']][c['municipio']]
    aver[c['departamento']][c['municipio']] = {}
  end
  if aver[c['departamento']][c['municipio']][c['vereda']]
    vrep += 1
    puts "\"#{c['cod_municipio']}\",\"#{c['departamento'].upcase}\",\"#{c['municipio'].upcase}\",#{c['cod_vereda']},\"#{c['vereda'].upcase}\", Nombre de vereda repetida en el mismo municipio por lo menos con código(s): #{aver[c['departamento']][c['municipio']][c['vereda']]} #{c['cod_vereda']}"
    aver[c['departamento']][c['municipio']][c['vereda']] += " #{c['cod_vereda']}"
  else
    aver[c['departamento']][c['municipio']][c['vereda']] = c['cod_vereda']
  end
  nver += 1
end
STDERR.puts "Creado árbol con veredas"
STDERR.puts "Veredas repetidas en el mismo municipio: #{vrep}"
STDERR.puts "Veredas agregadas: #{nver}"


sinv = 0
STDERR.puts "Buscando municipios sin veredas y veredas repetidas"
aver.each do |ad, rad|
  rad.each do |am, ram|
    if ram.count == 0
      puts "No se encontraron veredas en el municipio #{am}"
      sinv += 1
    end
  end
end

STDERR.puts "Municipios sin veredas: #{sinv}"

STDERR.puts "Verificando que haya veredas para todos los municipios del DIVIPOLA 2018"
mcub = 0
adiv.each do |ad, rad|
  if !aver[ad] 
    puts "No hay veredas para el departamento #{ad}"
  else
    rad.each do |am, ram|
      if !aver[ad][am]
        puts "No hay veredas para el municipio #{am} / #{ad}"
      else
        mcub += 1
      end
    end
  end
end
STDERR.puts "Municipios con alguna vereda: #{mcub}"


 

#!/usr/bin/env ruby
#
# Dominio público de acuerdo a la legislazción colombiana.
# vtamara@pasosdeJesus.org 2019
# Cuenta coincidencias precisas al buscar departamento, municipio, centro poblado del DIVIPOLA en descripciones de casos

require 'csv'
require 'byebug'


arcver = ARGV[0]
if !arcver
  puts "Falta archivo de veredas como primer parámetro"
  exit 1
end
arccasos = ARGV[1]
if !arccasos
  puts "Falta archivo de casos como segundo parámetro"
  exit 1
end
arcsal = ARGV[2]
if !arcsal
  puts "Falta archivo de salida como tercer parámetro"
  exit 1
end


def normaliza(texto)
  if texto.nil?
    return ''
  end
  r = texto.downcase.gsub('á', 'a')
  r.gsub!('Á', 'a')
  r.gsub!('é', 'e')
  r.gsub!('É', 'e')
  r.gsub!('í', 'i')
  r.gsub!('Í', 'i')
  r.gsub!('ó', 'o')
  r.gsub!('Ó', 'o')
  r.gsub!('ú', 'u')
  r.gsub!('Ú', 'u')
  r.gsub!('ü', 'u')
  r.gsub!('Ü', 'u')
  r
end

veredas = CSV.read(arcver, headers: true)
STDERR.puts "Se leyeron #{veredas.count} veredas"
veredas.each do |r|
  r['departamento'] = normaliza(r['departamento'])
  r['municipio'] = normaliza(r['municipio'])
  r['vereda'] = normaliza(r['vereda'])
end
puts "Datos veredales normalizados"

adiv = {} 
veredas.each do |c|
  if !adiv[c['departamento']]
    adiv[c['departamento']] = {}
    adiv[c['departamento']][c['municipio']] = {}
  elsif !adiv[c['departamento']][c['municipio']]
    adiv[c['departamento']][c['municipio']] = {}
  end
  adiv[c['departamento']][c['municipio']][c['vereda']] = 1
end
puts "Creados datos veredales estilo árbol"

casos = CSV.read(arccasos , headers: true)
STDERR.puts "Se leyeron #{casos.count} casos"
casos.each do |r|
  r['caso_id'] = r['id']  if r['id']
  r['memo'] = normaliza(r['text']) if r['text']
  r['memo_orig'] = r['text'] if r['text']
  r['departamento'] = normaliza(r['dep_coded']) if r['dep_coded']
  r['municipio'] = normaliza(r['mun_coded']) if r['mun_coded']
  r['vereda'] = normaliza(r['vereda']) if r['vereda']
end
puts "Casos normalizados"

numbc = 0
CSV.open(arcsal, "wb") do |csv|
  #csv << ["id", "descripcion", "departamento", "municipio", "vereda", "departamentoc", "municipioc", "veredac"]
  tcoinc = []
  ultpor = -1
  numcasos = 0
  casos.each do |caso|
    coinc = []
    adiv.keys.each do |dep|
      # Detectamos si el departamento aparece como palabra completa
      if caso['memo'] && (inddep=caso['memo'].index(dep)) &&
          (inddep == 0 || !/^[[:alnum:]]/.match(caso['memo'][inddep-1])) &&
          (inddep == caso['memo'].length-dep.length || !/^[[:alnum:]]/.match(caso['memo'][inddep+dep.length]))
        nivc = 'false'
        if (caso['departamento'] == dep) 
          if (caso['municipio'].nil?)
            nivc = 'true'
            numbc += 1
          else
            nivc = 'partial'
          end
        end
        csv << [caso['caso_id'], caso['memo_orig'], 
                caso['departamento'], caso['municipio'], 
                caso['vereda'], dep, nil, nil]
      end
      adiv[dep].keys.each do |mun|
        # Detectamos si el municipio aparece como palabra completa
        if caso['memo'] && (indmun=caso['memo'].index(mun)) &&
            (indmun == 0 || !/^[[:alnum:]]/.match(caso['memo'][indmun-1])) &&
            (indmun == caso['memo'].length-mun.length || !/^[[:alnum:]]/.match(caso['memo'][indmun+mun.length]))
          nivc = 'false'
          if (caso['municipio'] == mun) 
            if (caso['departamento'] == dep) 
              if (caso['vereda'].nil?)
                nivc = 'true'
                numbc += 1
              else
                nivc = 'partial'
              end
            end
          end
          csv << [caso['caso_id'], caso['memo_orig'], 
                  caso['departamento'], caso['municipio'], 
                  caso['vereda'], dep, mun, nil]
        end
        adiv[dep][mun].keys.each do |ver|
          # Detectamos si la vereda aparece como palabra completa
          if caso['memo'] && (indver=caso['memo'].index(ver)) &&
              (indver == 0 || !/^[[:alnum:]]/.match(caso['memo'][indver-1])) &&
              (indver == caso['memo'].length-ver.length || !/^[[:alnum:]]/.match(caso['memo'][indver+ver.length]))

            nivc = 'false'
            if (caso['vereda'] == ver)
              if (caso['municipio'] == mun &&
                  caso['departamento'] == dep)
                nivc = 'true'
                numbc += 1
              end
            end
            csv << [caso['caso_id'], caso['memo_orig'], 
                    caso['departamento'], 
                    caso['municipio'], 
                    caso['vereda'], 
                    dep, mun, ver]
          end
        end
      end
    end
    numcasos += 1
    pact = numcasos * 100 / casos.count
    if pact.to_i > ultpor
      ultpor = pact.to_i
      puts "#{ultpor}% Coinciden #{numbc} de #{numcasos} analizados"
    end
  end
end

puts "Número de registros con coincidencia correcta: #{numbc}"


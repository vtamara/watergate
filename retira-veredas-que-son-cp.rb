#!/usr/bin/env ruby
# encoding: utf-8
#
# Dominio público de acuerdo a la legislazción colombiana.
# vtamara@pasosdeJesus.org 2019
#
# Arregla archivo de veredas respecto a DIVIPOLA

require 'csv'
require 'byebug'


if ARGV.length != 3
  STDERR.puts "Primer argumento debe ser DIVIPOLA con campos departamento,municipio,centropoblado"
  STDERR.puts "Segundo debe ser archivo de veredas con campos departamento,municipio,vereda,cod_vereda"
  STDERR.puts "Tercero será archivo de salida"
  exit 1
end

ndiv = ARGV[0]
STDERR.puts "Leyendo divipola #{ndiv}"
divcsv = CSV.read(ndiv, headers: true)
encdiv = divcsv[0].headers
STDERR.puts "Se leyeron #{divcsv.count} registros de #{ndiv}"

adiv = {} 
divcsv.each do |c|
  if !adiv[c['departamento']]
    adiv[c['departamento']] = {}
    adiv[c['departamento']][c['municipio']] = {}
  elsif !adiv[c['departamento']][c['municipio']]
    adiv[c['departamento']][c['municipio']] = {}
  end
  adiv[c['departamento']][c['municipio']][c['centropoblado']] = c['cod_centropoblado'] 
end
STDERR.puts "Creado divipola estilo arbol"

nver = ARGV[1]
STDERR.puts "Leyendo archivo con veredas #{nver}"
vercsv = CSV.read(nver, headers: true)
encver = vercsv[0].headers
STDERR.puts "Se leyeron #{vercsv.count} registros de #{nver}"

STDERR.puts "Escribiendo en #{ARGV[2]}"

numsal = 0
vrep = 0
maldep = 0
malmun = 0
CSV.open(ARGV[2], "wb") do |csv|
  csv << encver
  vercsv.each do |r|
    if !adiv[r['departamento']]
      puts "#{r['cod_municipio']},\"#{r['departamento'].upcase}\",\"#{r['municipio'].upcase}\",#{r['cod_vereda']},\"#{r['vereda'].upcase}\",No se encontró departamento"
      maldep += 1
    elsif !adiv[r['departamento']][r['municipio']]
      puts "#{r['cod_municipio']},\"#{r['departamento']}\",\"#{r['municipio']}\",#{r['cod_vereda']},\"#{r['vereda'].upcase}\",No se encontró municipio"
      malmun += 1
    elsif adiv[r['departamento']][r['municipio']][r['vereda']]
      puts "#{r['cod_municipio']},\"#{r['departamento'].upcase}\",\"#{r['municipio'].upcase}\",#{r['cod_vereda']},\"#{r['vereda'].upcase}\",Centro poblado #{adiv[r['departamento']][r['municipio']][r['vereda']]} en DIVIPOLA tiene el mismo nombre de la vereda"
      vrep += 1
    else
      csv << r
      numsal += 1
    end
  end
end
STDERR.puts "Archivo generado con #{numsal} registros"
STDERR.puts "Departamentos no encontrados: #{maldep}"
STDERR.puts "Municipios no encontrados: #{malmun}"
STDERR.puts "Centros poblado con el mismo nombre de una vereda en el mismo municipio: #{vrep}"


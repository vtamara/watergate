#!/usr/bin/env ruby
# encoding: utf-8
#
# Dominio público de acuerdo a la legislazción colombiana.
# vtamara@pasosdeJesus.org 2019
# Analiza resultados enviados por Sheffield

require 'csv'
require 'byebug'


if ARGV.length != 1
  STDERR.puts "Se requiere como único argumento el resultado por analizar"
  exit 1
end

# Emula clase CSV pero para TSV
# Toma ideas de StrictTSV de https://stackoverflow.com/questions/4404787/whats-the-best-way-to-parse-a-tab-delimited-file-in-ruby/4404824
class TSV
  attr_reader :headers
  attr_reader :filepath
  attr_reader :registros

  def initialize(filepath, options)
    @filepath = filepath
    open(filepath)  do |f|
      if f.nil?
        STDERR.puts "No se puede leer #{filepath}"
        exit 1
      end
      if options[:headers]
        @headers = f.gets.strip.split("\t")
      else
        @headers = nil
        STDERR.puts "Se requieren encabezados en TSV"
      end
      @registros = []
      f.each do |l|
        z = @headers.zip(l.split("\t").map{|c| c == '' ? nil : c})
        @registros << Hash[z]
      end
    end
  end

  def each
    @registros.each do |r|
      yield r
    end
  end

  def count
    @registros.count
  end
end



def normaliza(texto)
  if texto.nil? || texto == '' || texto == 'null'
    return nil
  end
  r = texto.downcase.gsub('á', 'a')
  r.gsub!('Á', 'a')
  r.gsub!('é', 'e')
  r.gsub!('É', 'e')
  r.gsub!('í', 'i')
  r.gsub!('Í', 'i')
  r.gsub!('ó', 'o')
  r.gsub!('Ó', 'o')
  r.gsub!('ú', 'u')
  r.gsub!('Ú', 'u')
  r.gsub!('ü', 'u')
  r.gsub!('Ü', 'u')
  r
end


narch = ARGV[0]
STDERR.puts "Leyendo resultado #{narch}"
if narch[-3..-1] == 'csv'
  salida = CSV.read(narch, headers: true)
  encabezados = salida[0].headers
else
  salida = TSV.new(narch, headers: true)
  encabezados = salida.headers
end
STDERR.puts "Se leyeron #{salida.count} registros de #{narch}"


nent = 'memogeo_sivel12.csv'
STDERR.puts "Leyendo datos de entrada #{nent}"
casos = CSV.read(nent, headers: true)
casosid = {}
STDERR.puts "Se leyeron #{casos.count} casos de #{nent}"
casos.each do |r|
  r['memo'] = normaliza(r['memo']) if r['memo']
  r['departamento'] = normaliza(r['departamento']) if r['departamento']
  r['municipio'] = normaliza(r['municipio']) if r['municipio']
  r['centropoblado'] = normaliza(r['centropoblado']) if r['centropoblado']
  casosid[r['caso_id'].to_i] = r
end
STDERR.puts "#{casosid.count} casos de #{nent} normalizados"


STDERR.puts "Realizando conteos"
ultregconv = 0
ultregconf = 0
veredas = 0
casosvereda = 0
casosconf = 0
casosconf2 = 0
ultregconconf2 = 0

ultregconcomunidad = 0
casosconcomunidad = 0

ultregconpuerto = 0
casosconpuerto = 0

ultregconrio = 0
casosconrio = 0

casos = 0

faltaconf = 0

salida.each do |r|
  casos += 1
  cid= r['id'].to_i
  if !defined?(casosid[cid]) 
    STDERR.puts "En resultado hay código #{cid} que no corresponde a caso"
    next
  end
  if r['dep_coded'] && 
      casosid[cid]['departamento'] != normaliza(r['dep_coded']) #&&
      #(!casosid[cid]['departamento'].nil? || r['dep_coded'] != 'null')
    STDERR.puts "Resultado #{cid} tiene departamento #{r['dep_coded']} que no corresponde al del caso (#{casosid[cid]['departamento']})"
    next
  end
  if r['mun_coded'] && 
      casosid[cid]['municipio'] != normaliza(r['mun_coded']) #&&
      #(!casosid[cid]['municipio'].nil? || r['mun_coded'] != 'null')
    STDERR.puts "Resultado #{cid} tiene municipio #{r['mun_coded']} que no corresponde al del caso (#{casosid[cid]['municipio']})"
    next
  end
  if r['cp_coded'] && 
    casosid[cid]['centropoblado'] != normaliza(r['cp_coded']) #&&
      #(!casosid[cid]['centropoblado'].nil? || r['cp_coded'] != 'null')
    STDERR.puts "Resultado #{cid} tiene centro poblado #{r['cp_coded']} que no corresponde al del caso (#{casosid[cid]['centropoblado']})"
    next
  end

  encabezados.select {|x| x =~ /confirmed/}.each do |cv|
    if !r[cv].nil? && r[cv] == 'true'
      if (r['id'] != ultregconf)
        casosconf += 1
        ultregconf = r['id']
      end
    end
  end

  encabezados.select {|x| x =~ /departamento/}.each do |cv|
    #if cid == 100001
    #  byebug
    #end
    m = cv.sub('departamento', 'municipio')
    c = cv.sub('departamento', 'centro_poblado')
    #if !r[cv].nil? && r[cv] != '' && r[cv] != 'null'
#      if normaliza(r[cv]) == casosid[cid]['departamento'] &&
#          (normaliza(r[m]) == casosid[cid]['municipio']  || 
#           ((r[m] == '' || r[m] == 'null') && casosid[cid]['municipio'].nil?)) &&
#          (normaliza(r[c]) == casosid[cid]['centropoblado'] || 
#           ((r[c] == '' || r[c] == 'null') && casosid[cid]['centropoblado'].nil?)) &&
    if normaliza(r[cv]) == casosid[cid]['departamento'] &&
      normaliza(r[m]) == casosid[cid]['municipio'] &&
      normaliza(r[c]) == casosid[cid]['centropoblado'] &&
         ultregconconf2 != r['id']
        casosconf2 += 1
        ultregconconf2 = r['id']
        c1 = cv.sub('departamento', 'confirmed')
        #puts "#{cid} confirmado"
        if r[c1] != 'true'
          STDERR.puts "Comprobado caso #{cid} en grupo #{cv}, pero #{c1} no es true"
          faltaconf += 1
        end
      end
#    end
  end

  encabezados.select {|x| x =~ /comunidad/}.each do |cv|
    if !r[cv].nil? && r[cv] != '' && r[cv] != 'null'
      if (r['id'] != ultregconcomunidad)
        casosconcomunidad += 1
        ultregconcomunidad = r['id']
      end
    end
  end


  encabezados.select {|x| x =~ /vereda/}.each do |cv|
    if !r[cv].nil? && r[cv] != '' && r[cv] != 'null'
      veredas+=1
      if (r['id'] != ultregconv)
        casosvereda += 1
        ultregconv = r['id']
      end
    end
  end

  encabezados.select {|x| x =~ /puerto/}.each do |cv|
    if !r[cv].nil? && r[cv] != '' && r[cv] != 'null'
      if (r['id'] != ultregconrio)
        if (r['id'] != ultregconpuerto)
          casosconpuerto += 1
          ultregconpuerto = r['id']
        end
      end
    end
  end

  encabezados.select {|x| x =~ /rio/}.each do |cv|
    if !r[cv].nil? && r[cv] != '' && r[cv] != 'null'
      if (r['id'] != ultregconrio)
        casosconrio += 1
        ultregconrio = r['id']
      end
    end
  end


end


STDERR.puts "Registros: #{casos}"
puts "Veredas: #{veredas}"
puts "Casos con veredas: #{casosvereda}"
puts "Casos con comunidad: #{casosconcomunidad}"
puts "Casos con puerto: #{casosconpuerto}"
puts "Casos con rio: #{casosconrio}"
puts "Casos con confirmacion en hoja de cálculo: #{casosconf}"
puts "Casos con confirmacion comprobada: #{casosconf2}"
puts "Casos donde se encontró depto/mun/cp pero confirmado no es true: #{faltaconf}"


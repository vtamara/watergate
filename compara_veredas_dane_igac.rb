#!/usr/bin/env ruby

# Dominio público de acuerdo a la legislación colombiana.
# vtamara@pasosdeJesus.org 2021
#
# Analiza archivo con veredas DANE con las del IGAC

require 'csv'

if ARGV.length != 2
  STDERR.puts "Primer argumento debe ser archivo de veredas DANE"
  STDERR.puts "Segundo debe ser archivo de veredas IGAC"
  exit 1
end

def normaliza(texto)
  if texto.nil? || texto == '' || texto == 'null'
    return nil
  end
  r = texto.downcase.gsub('á', 'a')
  r.gsub!('Á', 'a')
  r.gsub!('é', 'e')
  r.gsub!('É', 'e')
  r.gsub!('í', 'i')
  r.gsub!('Í', 'i')
  r.gsub!('ó', 'o')
  r.gsub!('Ó', 'o')
  r.gsub!('ú', 'u')
  r.gsub!('Ú', 'u')
  r.gsub!('ü', 'u')
  r.gsub!('Ü', 'u')
  r
end


ndane = ARGV[0]
STDERR.puts "Leyendo veredas DANE de #{ndane}"
divcsv = CSV.read(ndane, headers: true)
encdiv = divcsv[0].headers
STDERR.puts "Se leyeron #{divcsv.count} registros de #{ndane}"

adiv = {} 
divcsv.each do |c|
  cmun = (c['cod_municipio'].to_s.length == 4 ? '0' : '') + c['cod_municipio'].to_s
  if !adiv[cmun]
    adiv[cmun] = {}
  end
  adiv[cmun][c['vereda']] = c['cod_vereda']
end
STDERR.puts "Creado árbol"

nigac = ARGV[1]
STDERR.puts "Leyendo archivo con veredas #{nigac}"
verigac = CSV.read(nigac, headers: true)
encverigac = verigac[0].headers
STDERR.puts "Se leyeron #{verigac.count} registros de #{nigac}"

mnoenc = {}
msienc = {}
vnoenc = {}
vsienc = {}
adiv2 = {}
malmun = 0
malver = 0
nreg = 0
verigac.each do |r|
  nreg += 1
# Sin normalizar
# r['departamento'] = normaliza(r['departamento'])
#  r['municipio'] = normaliza(r['municipio'])
#  r['vereda'] = normaliza(r['vereda'])
  codsector = r['sector_cod,C,9']
  codmun = codsector[0..4]
  nomver = r['nombre,C,100']
  codver = r['codigo,C,17']
  codant = r['codigo_ant,C,13']
  if !adiv[codmun]
    if !mnoenc[codmun]
      puts "\"#{codver}\",\"#{codsector}\",\"#{nomver}\",\"#{codant}\",\"No se encontró código de municipio\""
      mnoenc[codmun] = codmun
      vnoenc[codver] = nomver
    end
  else 
    if !msienc[codmun]
      msienc[codmun] = codmun
    end
    if !adiv[codmun][nomver]
      puts "\"#{codver}\",\"#{codsector}\",\"#{nomver}\",\"#{codant}\",\"No se encontró vereda en municipio\""
      vnoenc[codver] = nomver
    else # Si está municipio y si está vereda
      vsienc[codver] = nomver
    end
  end
  if !adiv2[codmun]
    adiv2[codmun] = {}
  end
  adiv2[codmun][nomver] = codver
end

malmun2 = 0
malver2 = 0
totv = 0
enc = 0
adiv.keys.each do |cm|
  if !adiv2[cm]
    puts "En IGAC no hay municipio con código #{cm}"
    malmun += 1
  else
    adiv[cm].each do |nv, cv|
      totv += 1
      if !adiv2[cm][nv]
        puts "En IGAC no hay vereda #{nv} en municipio #{cm}"
        malver2 += 1
      else
        enc += 1
      end
    end
  end
end


STDERR.puts "Veredas leidas"  
STDERR.puts "Códigos de municipios de IGAC no encontrados en DANE: #{mnoenc.size}"
STDERR.puts "Códigos de municipios de IGAC si encontrados en DANE: #{msienc.size}"
STDERR.puts "Nombres de veredas de IGAC no encontradas en DANE #{vnoenc.size}"
STDERR.puts "Nombres de veredas de IGAC si encontradas en DANE #{vsienc.size}"
STDERR.puts "Municipios de DANE no encontrados en IGAC: #{malmun2}"
STDERR.puts "Veredas de DANE no encontradas en IGAC #{malver2}"




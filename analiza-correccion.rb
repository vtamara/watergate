#!/usr/bin/env ruby
# encoding: utf-8
#
# Dominio público de acuerdo a la legislazción colombiana.
# vtamara@pasosdeJesus.org 2019
#
# Analiza corrección manual a Datos de Sheffield

require 'csv'
require 'byebug'


if ARGV.length != 3
  STDERR.puts "Primer argumento debe ser DIVIPOLA con campos departamento,municipio,centropoblado"
  STDERR.puts "Segundo argumento debe ser archivo con veredas con campos departamento,municipio,centropoblado,vereda"
  STDERR.puts "Tercero debe ser archivo con corrección de datos con campos departamento, municipio, centropoblado, departamento_corregido, municipio_corregido, centropoblado_corregido,vereda_corregida,observaciones_corregida,estado_corregido"
  exit 1
end

def normaliza(texto)
  if texto.nil? || texto == '' 
    return nil
  end
  r = texto.downcase.gsub('á', 'a')
  r.gsub!('Á', 'a')
  r.gsub!('é', 'e')
  r.gsub!('É', 'e')
  r.gsub!('í', 'i')
  r.gsub!('Í', 'i')
  r.gsub!('ó', 'o')
  r.gsub!('Ó', 'o')
  r.gsub!('ú', 'u')
  r.gsub!('Ú', 'u')
  r.gsub!('ü', 'u')
  r.gsub!('Ü', 'u')
  r
end


ndiv = ARGV[0]
STDERR.puts "Leyendo divipola #{ndiv}"
divcsv = CSV.read(ndiv, headers: true)
encdiv = divcsv[0].headers
STDERR.puts "Se leyeron #{divcsv.count} registros de #{ndiv}"
divcsv.each do |r|
  r['departamento'] = normaliza(r['departamento'])
  r['municipio'] = normaliza(r['municipio'])
  r['centropoblado'] = normaliza(r['centropoblado'])
end
STDERR.puts "Divipola normalizado"

adiv = {} 
divcsv.each do |c|
  if !adiv[c['departamento']]
    adiv[c['departamento']] = {}
    adiv[c['departamento']][c['municipio']] = {}
  elsif !adiv[c['departamento']][c['municipio']]
    adiv[c['departamento']][c['municipio']] = {}
  end
  adiv[c['departamento']][c['municipio']][c['centropoblado']] = c['cod_centropoblado'] 
end
STDERR.puts "Creado divipola estilo arbol"

nver = ARGV[1]
STDERR.puts "Leyendo archivo con veredas #{nver}"
vercsv = CSV.read(nver, headers: true)
encver = vercsv[0].headers
STDERR.puts "Se leyeron #{vercsv.count} registros de #{nver}"

maldep = 0
malmun = 0
vrep = 0
nver = 0
nreg = 0
vercsv.each do |r|
  nreg += 1
  r['departamento'] = normaliza(r['departamento'])
  r['municipio'] = normaliza(r['municipio'])
  r['vereda'] = normaliza(r['vereda'])
  if !adiv[r['departamento']]
    STDERR.puts "#{r['cod_municipio']},\"#{r['departamento'].upcase}\",\"#{r['municipio'].upcase}\",#{r['cod_vereda']},\"#{r['vereda'].upcase}\",No se encontró departamento"
    maldep += 1
  elsif !adiv[r['departamento']][r['municipio']]
    STDERR.puts "#{r['cod_municipio']},\"#{r['departamento']}\",\"#{r['municipio']}\",#{r['cod_vereda']},\"#{r['vereda'].upcase}\",No se encontró municipio"
    malmun += 1
  elsif adiv[r['departamento']][r['municipio']][r['vereda']]
    STDERR.puts "#{r['cod_municipio']},\"#{r['departamento'].upcase}\",\"#{r['municipio'].upcase}\",#{r['cod_vereda']},\"#{r['vereda'].upcase}\",Centro poblado #{adiv[r['departamento']][r['municipio']][r['vereda']]} en DIVIPOLA tiiene el mismo nombre de la vereda"
    vrep += 1
  end
end
STDERR.puts "Veredas normalizadas"  
STDERR.puts "Departamentos no encontrados: #{maldep}"
STDERR.puts "Municipios no encontrados: #{malmun}"
STDERR.puts "Centros poblado con el mismo nombre de una vereda en el mismo municipio: #{vrep}"

aver = {} 
nreg = 0
vrep = 0
vercsv.each do |c|
  nreg += 1
  if !aver[c['departamento']]
    aver[c['departamento']] = {}
    aver[c['departamento']][c['municipio']] = {}
  elsif !aver[c['departamento']][c['municipio']]
    aver[c['departamento']][c['municipio']] = {}
  end
  if aver[c['departamento']][c['municipio']][c['vereda']]
    vrep += 1
    STDERR.puts "\"#{c['cod_municipio']}\",\"#{c['departamento'].upcase}\",\"#{c['municipio'].upcase}\",#{c['cod_vereda']},\"#{c['vereda'].upcase}\", Nombre de vereda repetida en el mismo municipio por lo menos con código(s): #{aver[c['departamento']][c['municipio']][c['vereda']]} #{c['cod_vereda']}"
    aver[c['departamento']][c['municipio']][c['vereda']] += " #{c['cod_vereda']}"
  else
    aver[c['departamento']][c['municipio']][c['vereda']] = c['cod_vereda']
  end
  nver += 1
end
STDERR.puts "Creado árbol con veredas"
STDERR.puts "Veredas repetidas en el mismo municipio: #{vrep}"
STDERR.puts "Veredas agregadas: #{nver}"


sinv = 0
STDERR.puts "Buscando municipios sin veredas y veredas repetidas"
aver.each do |ad, rad|
  rad.each do |am, ram|
    if ram.count == 0
      STDERR.puts "No se encontraron veredas en el municipio #{am}"
      sinv += 1
    end
  end
end

STDERR.puts "Municipios sin veredas: #{sinv}"


ncor = ARGV[2]
STDERR.puts "Leyendo archivo con correcciones #{ncor}"
corcsv = CSV.read(ncor, headers: true)
enccor = corcsv[0].headers
STDERR.puts "Se leyeron #{corcsv.count} registros de #{ncor}"

def reporta_error_cor(c, mens)
  puts c['id'] + ',' + mens
end

ncor = 0
corcsv.each do |c|
  ncor += 1

  di = normaliza(c['departamento_inicial'])
  mi = normaliza(c['municipio_inicial'])
  ci = normaliza(c['centropoblado_inicial'])
  dc = normaliza(c['departamento_corregido'])
  mc = normaliza(c['municipio_corregido'])
  cc = normaliza(c['centropoblado_corregido'])
  vc = normaliza(c['vereda_corregida'])
  if !adiv[di]
    reporta_error_cor c, "Departamento inicial '#{di}' no está en archivo DIVIPOLA"
  elsif !adiv[di][mi]
    reporta_error_cor c, "Municipio inicial '#{mi} #{di}' no está en archivo DIVIPOLA"
  elsif ci && ci != 'null' && !adiv[di][mi][ci]
    reporta_error_cor c, "Centro poblado inicial '#{ci} #{mi} #{di}' no está en DIVIPOLA"
  end
  if (dc && !aver[dc])
    reporta_error_cor c, "Departamento corregido '#{dc}' no está en archivo de veredas"
  elsif (mc && dc && !aver[dc][mc])
    reporta_error_cor c, "Municipio corregido '#{mi} #{dc}' no está en archivo de veredas"
  elsif (mc && !dc && !aver[di][mc])
    reporta_error_cor c, "Municipio corregido '#{mc} #{di}' no está en archivo de veredas"
  elsif cc && cc != 'null' && mc && dc && !adiv[dc][mc][cc]
    reporta_error_cor c, "Centro poblado corregido '#{cc} #{mc} #{dc}' no está en DIVIPOLA"
  elsif cc && cc != 'null' && mc && !dc && !adiv[di][mc][cc]
    reporta_error_cor c, "Centro poblado corregido '#{cc} #{mc} #{di}' no está DIVIPOLA"
  elsif cc && cc != 'null' && !mc && !dc && adiv[di] && adiv[di][mi] && !adiv[di][mi][cc] && aver[di][mi] && !aver[di][mi][cc]
    reporta_error_cor c, "Centro poblado corregido '#{cc} #{mi} #{di}' tampoco está en archivo de veredas"
  elsif (!cc || cc == 'null') && vc && mc && dc && aver[dc][mc] && !aver[dc][mc][cc]
    reporta_error_cor c, "Vereda corregida '#{vc} #{mc} #{dc}' no está en archivo de veredas"
  elsif (!cc || cc == 'null') && vc && mc && !dc && !aver[di][mc][cc]
    reporta_error_cor c, "Vereda corregida '#{vc} #{mc} #{di}' no está DIVIPOLA"
  elsif (!cc || cc == 'null') && vc && !mc && !dc && aver[di] && aver[di][mi] && !aver[di][mi][cc]
    reporta_error_cor c, "Vereda corregida '#{vc} #{mi} #{di}' no está en archivo de veredas"
  end
end


STDERR.puts "Revisados #{ncor} registros corregidos"

#!/usr/bin/env ruby
# encoding: utf-8
#
# Dominio público de acuerdo a la legislazción colombiana.
# vtamara@pasosdeJesus.org 2019
# Cuenta coincidencias precisas al buscar departamento, municipio, centro poblado del DIVIPOLA en descripciones de casos

require 'csv'
require 'byebug'

def normaliza(texto)
  r = texto.downcase.gsub('á', 'a')
  r.gsub!('Á', 'a')
  r.gsub!('é', 'e')
  r.gsub!('É', 'e')
  r.gsub!('í', 'i')
  r.gsub!('Í', 'i')
  r.gsub!('ó', 'o')
  r.gsub!('Ó', 'o')
  r.gsub!('ú', 'u')
  r.gsub!('Ú', 'u')
  r.gsub!('ü', 'u')
  r.gsub!('Ü', 'u')
  r
end

divipola = CSV.read("divipola_sivel12.csv", headers: true)
STDERR.puts "Se leyeron #{divipola.count} centros poblados de Divipola"
divipola.each do |r|
  r['departamento'] = normaliza(r['departamento'])
  r['municipio'] = normaliza(r['municipio'])
  r['centropoblado'] = normaliza(r['centropoblado'])
end
puts "Divipola normalizado"

adiv = {} 
divipola.each do |c|
  if !adiv[c['departamento']]
    adiv[c['departamento']] = {}
    adiv[c['departamento']][c['municipio']] = {}
  elsif !adiv[c['departamento']][c['municipio']]
    adiv[c['departamento']][c['municipio']] = {}
  end
  adiv[c['departamento']][c['municipio']][c['centropoblado']] = 1
end
puts "Creado divipola estilo arbol"

casos = CSV.read("memogeo_sivel12.csv", headers: true)
STDERR.puts "Se leyeron #{casos.count} casos"
casos.each do |r|
  r['memo'] = normaliza(r['memo']) if r['memo']
  r['departamento'] = normaliza(r['departamento']) if r['departamento']
  r['municipio'] = normaliza(r['municipio']) if r['municipio']
  r['centropoblado'] = normaliza(r['centropoblado']) if r['centropoblado']
end
puts "Casos normalizados"

numbc = 0
CSV.open("salida-4.csv", "wb") do |csv|
  csv << ["id", "descripcion", "departamento", "municipio", "centropoblado", "departamentoc", "municipioc", "centropobladoc", "confirmado"]
  tcoinc = []
  ultpor = -1
  numcasos = 0
  casos.each do |caso|
    coinc = []
    adiv.keys.each do |dep|
      if caso['memo'].include?(dep)
        nivc = 'false'
        if (caso['departamento'] == dep) 
          if (caso['municipio'].nil?)
            nivc = 'true'
            numbc += 1
          else
            nivc = 'partial'
          end
        end
        csv << [caso['caso_id'], caso['memo'], 
      caso['departamento'], caso['municipio'], 
      caso['centropoblado'], dep, nil, nil, nivc]
      end
      adiv[dep].keys.each do |mun|
        if caso['memo'].include?(mun)
          nivc = 'false'
          if (caso['municipio'] == mun) 
            if (caso['departamento'] == dep) 
              if (caso['centropoblado'].nil?)
                nivc = 'true'
                numbc += 1
              else
                nivc = 'partial'
              end
            end
          end
          csv << [caso['caso_id'], caso['memo'], 
       		caso['departamento'], caso['municipio'], 
       		caso['centropoblado'], dep, mun, nil, nivc]
        end
        adiv[dep][mun].keys.each do |cp|
          if caso['memo'].include?(cp)
            nivc = 'false'
            if (caso['centropoblado'] == cp)
              if (caso['municipio'] == mun &&
                  caso['departamento'] == dep)
                nivc = 'true'
                numbc += 1
              end
            end
            csv << [caso['caso_id'], caso['memo'], 
        	caso['departamento'], 
        	caso['municipio'], 
        	caso['centropoblado'], 
        	dep, mun, cp, nivc]
          end
        end
      end
    end
    numcasos += 1
    pact = numcasos*100/casos.count
    if pact.to_i > ultpor
      ultpor = pact.to_i
      puts "#{ultpor}% Coinciden #{numbc} de #{numcasos} analizados"
    end
  end
end

puts "Número de registros con coincidencia correcta: #{numbc}"




Scripts para analizar y comparar información geográfica

Datos incluidos:

* Datos geográficos abiertos
  * CRVeredas_2017.csv  DANE, ver https://github.com/pasosdeJesus/sip/tree/datos/geografia
  * CRVeredas_2017_arreglada_DIVIPOLA_2018_sin_cp_rep.csv  DANE, ver https://github.com/pasosdeJesus/sip/tree/datos/geografia
  * R_VEREDA_IGAC_2021-11.csv IGAC Nov.2021, ver https://geoportal.igac.gov.co/contenido/datos-abiertos-catastro
  * divipola_DANE_2018.csv DANE, proviend de https://geoportal.dane.gov.co/metadatos/historicos/archivos/Listado_2018.xls con copia en https://github.com/pasosdeJesus/sip/blob/datos/geografia/Listado_2018.xlsx

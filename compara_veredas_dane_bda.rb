#!/usr/bin/env ruby

# Dominio público de acuerdo a la legislación colombiana.
# vtamara@pasosdeJesus.org 2021
#
# Compara archivo con veredas DANE con verificadas de BD por Anyelith

require 'byebug'
require 'csv'

if ARGV.length != 2
  STDERR.puts "Primer argumento debe ser archivo de veredas DANE"
  STDERR.puts "Segundo debe ser archivo de veredas BD"
  exit 1
end

def normaliza(texto)
  if texto.nil? || texto == '' || texto == 'null'
    return nil
  end
  r = texto.downcase.gsub('á', 'a')
  r.gsub!('Á', 'a')
  r.gsub!('é', 'e')
  r.gsub!('É', 'e')
  r.gsub!('í', 'i')
  r.gsub!('Í', 'i')
  r.gsub!('ó', 'o')
  r.gsub!('Ó', 'o')
  r.gsub!('ú', 'u')
  r.gsub!('Ú', 'u')
  r.gsub!('ü', 'u')
  r.gsub!('Ü', 'u')
  r
end


ndane = ARGV[0]
STDERR.puts "Leyendo veredas DANE de #{ndane}"
danecsv = CSV.read(ndane, headers: true)
encdane = danecsv[0].headers
STDERR.puts "Se leyeron #{danecsv.count} registros de #{ndane}"

div1 = {} 
danecsv.each do |c|
  dep = c['departamento']
  mun = c['municipio']
  ver = c['vereda']
  codver = (c['cod_vereda'].length <= 7 ? '0' : '') + c['cod_vereda']

  if !div1[dep]
    div1[dep] = {}
  end
  if !div1[dep][mun]
    div1[dep][mun] = {}
  end
  div1[dep][mun][ver] = codver
end
STDERR.puts "Creado árbol"

nbda = ARGV[1]
STDERR.puts "Leyendo archivo con veredas #{nbda}"
verbda = CSV.read(nbda, headers: true)
encverbda = verbda[0].headers
STDERR.puts "Se leyeron #{verbda.count} registros de #{nbda}"

dnoenc = {}
dsienc = {}
mnoenc = {}
msienc = {}
vnoenc = {}
vsienc = {}
div2 = {}
malmun = 0
malver = 0
nreg = 0
verbda.each do |r|
  nreg += 1
  dep = r['departamento'].to_s.to_s
  mun = r['municipio'].to_s
  ver = r['vereda'].to_s.upcase
  puts nreg
  if !div1[dep]
    if !dnoenc[dep]
      puts "\"#{dep}\",\"#{mun}\",\"#{ver}\",\"No se encontró departamento\""
      dnoenc[dep] = []
    end
    mnoenc[dep] = {} if !mnoenc[dep]
    mnoenc[dep][mun] = [] if !mnoenc[dep][mun]
    vnoenc[dep] = {} if !vnoenc[dep]
    vnoenc[dep][mun] = {} if !vnoenc[dep][mun]
    vnoenc[dep][mun][ver] = [] if !vnoenc[dep][mun][ver]

    dnoenc[dep] += [nreg]
    mnoenc[dep][mun] += [nreg]
    vnoenc[dep][mun][ver] += [nreg]
  else 
    dsienc[dep] = dep
    if !div1[dep][mun]
      puts "\"#{dep}\",\"#{mun}\",\"#{ver}\",\"No se encontró municipio\""
      if !mnoenc[dep] 
        mnoenc[dep] = {}
        vnoenc[dep] = {}
      end
      if !mnoenc[dep][mun]
        mnoenc[dep][mun] = []
        vnoenc[dep][mun] = {}
      end
      mnoenc[dep][mun] += [nreg]
      if !vnoenc[dep][mun][ver]
        vnoenc[dep][mun][ver] = []
      end
      vnoenc[dep][mun][ver] += [nreg]
    else # Si están departamento y municipio
      if !msienc[dep]
        msienc[dep] = {}
      end
      msienc[dep][mun] = mun
      if !div1[dep][mun][ver]
        puts "\"#{dep}\",\"#{mun}\",\"#{ver}\",\"No se encontró vereda\""
        if !vnoenc[dep]
          vnoenc[dep] = {}
        end
        if !vnoenc[dep][mun]
          vnoenc[dep][mun] = {}
        end
        if !vnoenc[dep][mun][ver]
          vnoenc[dep][mun][ver] = []
        end
        vnoenc[dep][mun][ver] += [nreg]
      else
        if !vsienc[dep]
          vsienc[dep] = {}
        end
        if !vsienc[dep][mun]
          vsienc[dep][mun] = {}
        end
        vsienc[dep][mun][ver] = ver
      end
    end
  end
  if !div2[dep]
    div2[dep] = {}
  end
  if !div2[dep][mun]
    div2[dep][mun] = {}
  end
  if !div2[dep][mun][ver]
    div2[dep][mun][ver] = ver
  end
end

STDERR.puts "Veredas leidas"  
STDERR.puts "Departamentos de BDA no encontrados en DANE: #{dnoenc.size}. #{dnoenc.keys}"
STDERR.puts "Departamentos de BDA si encontrados en DANE: #{dsienc.size}"
lmn = []
mnoenc.each {|d,vd| vd.each {|m,vm| lmn << "#{m} / #{d}"}}
STDERR.puts "Municipios de BDA no encontrados en DANE: #{lmn.size}."
lmn.each {|md| STDERR.puts "  #{md}"}
lms = []
msienc.each {|d,vd| vd.each {|m,vm| lms << "#{m} / #{d}"}}
STDERR.puts "Municipios de BDA si encontrados en DANE: #{lms.size}. "
lvn = []
vnoenc.each {|d,vd| vd.each {|m,vm| vm.each {|v,vv|  lvn << "#{v} / #{m} / #{d}"}}}
STDERR.puts "Veredas de BDA no encontrados en DANE: #{lvn.size}."
lvs = []
vsienc.each {|d,vd| vd.each {|m,vm| vm.each {|v,vv|  lvs << "#{v} / #{m} / #{d}"}}}
STDERR.puts "Veredas de BDA si encontrados en DANE: #{lvs.size}. "
byebug

maldep2 = 0
malmun2 = 0
malver2 = 0
totv = 0
enc = 0
div1.keys.each do |dep|
  if !div2[dep]
    puts "En BDA no hay departamento #{dep}"
    maldep2 += 1
  else
    div1[dep].keys.each do |mun|
      if !div2[dep][mun]
        puts "En BDA no hay municipio #{mun} en departamento #{dep}"
        malmun2 += 1
      else
        div1[dep][mun].keys.each do |ver|
          if !div2[dep][mun][ver]
            puts "En BDA no hay vereda #{ver} en municipio #{dep}, #{mun}"
            malver2 += 1
          end
        end
      end
    end
  end
end


lm = ''
mnoenc.each { |d,ms|
  ms.each {|m, l| lm += d + " / " + m + ".  "}
}
STDERR.puts lm
#byebug
STDERR.puts "Veredas de BDA no encontrados en DANE: #{vnoenc.size}. #{vnoenc.keys}"
STDERR.puts "Veredas de BDA si encontrados en DANE: #{vsienc.size}"
STDERR.puts "Departamentos de DANE no encontrados en BDA: #{maldep2}"
STDERR.puts "Municipios de DANE no encontrados en BDA: #{malmun2}"
STDERR.puts "Veredas de DANE no encontradas en BDA: #{malver2}"



